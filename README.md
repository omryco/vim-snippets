# Vim Snippets

Copied from https://github.com/keelii/vim-snippets.

Added `tex` snippets from https://github.com/gillescastel/latex-snippets.
